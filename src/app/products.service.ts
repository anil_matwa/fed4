import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MyProducts } from './MyProducts';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  private url: string = "http://localhost:3000/products";

  getProduct() {
    return this.http.get<MyProducts[]>(this.url);
  }
}
