import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router ,Params} from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  name:string;
  constructor(private router: Router,private route:ActivatedRoute) { }

  ngOnInit(): void {
    // this.name=this.route.snapshot.params['name'];
    this.route.params.subscribe((para:Params) =>
    {
      this.name=para['name'];
    })
  }

  goBack() {
    this.router.navigate(['products']);
  }

 
}
