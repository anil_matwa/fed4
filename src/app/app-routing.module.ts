import { Component, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductDetailComponent } from './products/product-detail/product-detail.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
   {path: ''  , component: AboutComponent},
  { path: '',   redirectTo: '/about', pathMatch: 'full' },
  { path: 'products' , component: ProductsComponent},
  {path: 'Products/:name',component: ProductDetailComponent},
  {path: '**' , component: PageNotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    FormsModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
