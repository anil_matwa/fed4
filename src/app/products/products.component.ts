import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  sub: any;
  id: number;

  constructor(private http: HttpClient,private prodservice: ProductsService ,
    private actRoute: ActivatedRoute, private router:Router) {}

  public name: string;
  public quantity: number;
  
  public ad=[];
  
  ngOnInit(): void {
    this.prodservice.getProduct().subscribe((res) => this.ad = res);
  
}

// ngOnDestroy() {
//   this.sub.unsubscribe();
// }
  
  displayedColumns: string[] = ['name','quantity'];
  
  addtoTable() {
    var custdto:any = {};
    custdto.name=this.name;
    custdto.quantity=this.quantity;
    this.http.post("http://localhost:3000/products",custdto).subscribe(res =>this.ad)

    this.ngOnInit();
}


myFunction(id) {
  var retVal = confirm("Are you sure you want to view the details ?");
  if( retVal == true ) {
    this.router.navigate(['Products',id]);
     return true;
  } else {
    this.router.navigate(['products']);
     return false;
  }}

}
